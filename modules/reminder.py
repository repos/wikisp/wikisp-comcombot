from datetime import date, timedelta

import pandas as pd
from openpyxl import load_workbook
from telegram.ext import CallbackContext

from modules import tools


async def reminder(context: CallbackContext):
    """
    This function sends a reminder message to a specified chat_id
    by downloading a file, processing it, and sending the result as a message.
    """

    # Define the URL of the file to be downloaded
    file_url = "https://cloud.wikisp.org/s/gddewqy6JmoDn9o/download/Cronograma.xlsx"

    # Download the file using a helper function
    file = tools.download_file(file_url)

    # Process the downloaded file to generate the reminder message
    message = process_file_reminder(file)

    # Send the reminder message to the specified chat_id
    await context.bot.send_message(
        chat_id="-1001867347350",
        text=message,
    )


def set_message(today, data):
    """
    Generates a message based on the given date and data.

    Args:
        today (datetime): The current date.
        data (pd.DataFrame): The data containing the relevant information.

    Returns:
        str: The generated message.
    """
    # Get the data for the current week, last week, and next week
    last_week = data[data["Fecha"] == pd.to_datetime(today)].values.tolist()
    this_week = data[
        data["Fecha"] == (pd.to_datetime(today) + timedelta(days=1))
    ].values.tolist()
    next_week = data[
        data["Fecha"] == (pd.to_datetime(today) + timedelta(days=8))
    ].values.tolist()

    message = ""

    if today.isoweekday() == 5:
        # Format the message for Friday
        message = """¡Gracias por tu labor, {}! ☺️
{}, ¡es tu turno de brillar! 🤩
Si requieres apoyo en flyers, organiza con {}.
{} por favor, prepara tu contenido para la siguiente semana, ¡gracias!"""
        message = message.format(
            last_week[0][6], this_week[0][6], this_week[0][8], next_week[0][6]
        )

    elif today.isoweekday() == 3:
        # Format the message for Wednesday
        message = """¡Ha llegado el miércoles! Recordatorio amable de que está
finalizando la semana. ¡Tu momento de brillar se acerca, {}!"""
        message = message.format(next_week[0][6])

    return message


def process_file_reminder(file):
    """
    Process the given file and return a reminder message.

    Args:
        file (str): The path to the file to be processed.

    Returns:
        str: The reminder message.
    """
    # Load the workbook from the given file
    book = load_workbook(filename=file)

    # Get the sheet named "2023"
    sheet = book["2023"]

    # Convert the sheet data into a dataframe
    data = tools.get_as_dataframe(sheet)

    # Get the current date
    today = date.today()

    # Generate the reminder message using the current date and the data
    return set_message(today, data)
