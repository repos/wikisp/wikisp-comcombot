import datetime
from datetime import date

from openpyxl import load_workbook
from telegram.ext import CallbackContext

from modules import tools


async def birthday(context: CallbackContext):
    """
    An asynchronous function that downloads a file and generates a birthday message
    that is sent to a group.

    Args:
        context (CallbackContext): The context object for the callback function.

    Returns:
        None
    """

    # Define the URL from which to download the file
    url = "https://cloud.wikisp.org/s/Q4fS7wjFGQcXaQf/download/cumples.xlsx"

    # Download the file
    file = tools.download_file(url)

    # Process the downloaded file to generate a birthday message
    message, today_has_birthdays = process_file_birthday(file)

    # Check if there are birthdays today
    if today_has_birthdays:
        # Send a birthday image
        await context.bot.sendPhoto(
            chat_id="-1001173892862",
            photo=open(
                "/data/project/wikisp/wikisp-comcombot/img/felizcumple.png", "rb"
            ),
        )

    # Send the birthday message to a specific chat ID
    if message:
        await context.bot.send_message(
            chat_id="-1001173892862",
            text=message,
        )


def process_file_birthday(file):
    """
    Processes an Excel file and returns a message based on the data in the file.

    Args:
        file (str): The filename of the Excel file.

    Returns:
        tuple: A tuple containing:
            - str: A message based on the data in the file.
            - bool: True if there are birthdays today, False otherwise.
    """
    # Load the workbook from the file
    workbook = load_workbook(filename=file)

    # Get the specific worksheet named "cumples"
    worksheet = workbook["cumples"]

    # Get the data from the worksheet as a dataframe
    data = tools.get_as_dataframe(worksheet)

    # Get today's date
    today = date.today()

    # Return a message based on the birthday member for today's date
    msg, today_has_birthdays = get_birthday_member(today, data)

    return msg, today_has_birthdays


def get_birthday_member(today, data):
    """
    Get a list of names of people who have their birthday today.

    Args:
        today (datetime.date): Today's date.
        data (pd.DataFrame): Data of the sheet.

    Returns:
        tuple: A tuple containing the birthday message and a boolean indicating whether there are birthdays today.
    """
    # Format today's date as "day-month"
    today = today.strftime("%d-%m")

    # Filter the data to get people with birthdays today
    birthday_people = data[data["Fecha"].str.contains(today)]

    # Initialize variables
    msg = False
    today_has_birthdays = False

    # If there are people with birthdays today, call the set_birthday_messages function
    if not birthday_people.empty:
        msg, today_has_birthdays = set_birthday_messages(birthday_people)

    return msg, today_has_birthdays


def set_birthday_messages(birthday_people):
    """
    Generates birthday messages for different types of celebrations.

    Args:
        birthday_people (pandas.DataFrame): DataFrame containing information about people and their celebrations.

    Returns:
        str: Birthday messages for the given celebrations.
    """
    # Filter birthday_people for different types of celebrations
    birthdays = birthday_people[birthday_people["Tipo"] == "Cumpleaños"]
    wikibirthdays = birthday_people[birthday_people["Tipo"] == "Wikicumpleaños"]
    aniversaries = birthday_people[birthday_people["Tipo"] == "Aniversario"]

    # Generate birthday messages
    msg = ""
    today_has_birthdays = False
    if not birthdays.empty:
        # Get names of birthdayers
        birthdayers = birthdays["Nombre"].values.tolist()

        if len(birthdayers) > 1:
            # Generate message for multiple birthdayers
            names = set_names(birthdayers)
            msg = f"Hoy están de cumpleaños {names}. Muchas felicidades en vuestro día. 🥳\n\n"
        else:
            # Generate message for a single birthdayer
            msg = f"Hoy está de cumpleaños {birthdayers[0]}. Muchas felicidades en tu día. 🥳\n\n"
        today_has_birthdays = True

    if not wikibirthdays.empty:
        # Get names of wikibirthdayers
        birthdayers = wikibirthdays["Nombre"].values.tolist()

        if len(birthdayers) > 1:
            # Generate message for multiple wikibirthdayers
            names = set_names(birthdayers)
        else:
            # Generate message for a single wikibirthdayer
            names = birthdayers[0]

        msg += (
            f"Hoy es el wikicumpleaños de {names}. Gracias por el trabajo dedicado "
            + "a la comunidad de los proyectos Wikimedia. 🥳\n\n"
        )

    if not aniversaries.empty:
        # Calculate years since the anniversary
        years = int(datetime.date.today().year) - int(aniversaries["Año"].values[0])

        msg += (
            f"Hoy, hace {years} años fue fundado {aniversaries['Nombre'].values[0]}. "
            + "Celebremos tanto su aniversario como su innegable contribución al conocimiento libre."
        )

    return msg, today_has_birthdays


def set_names(birthdayers):
    """
    Concatenates the names of the birthdayers and separates them with commas and ' y '.

    Args:
        birthdayers (list): A list of names.

    Returns:
        str: A string containing the names of the birthdayers separated by commas and ' y '.
    """
    # Join all names except the last one with ', '
    names = [", ".join(birthdayers[:-1])]

    # Append the last name
    names.append(birthdayers[-1])

    # Join all names with ' y '
    wmsp_birthdays = " y ".join(names)

    return wmsp_birthdays
