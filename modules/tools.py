from itertools import islice

import pandas as pd
import requests


def get_as_dataframe(sheet):
    """
    Extracts the values from a spreadsheet and returns them as a dataframe.

    Args:
        sheet (str): The worksheet.

    Returns:
        pandas.DataFrame: The data of the sheet as a dataframe.
    """
    # Get the values from the sheet
    data = sheet.values

    # Get the column names
    cols = next(data)

    # Convert the data to a list of lists
    data = list(data)

    # Remove any empty rows from the data
    data = (islice(r, None, None) for r in data)

    # Create a dataframe from the data with the column names
    return pd.DataFrame(data, columns=cols)


def download_file(url=None):
    """
    Downloads a file from the given URL and returns the name of the downloaded file.

    Args:
        url (str): The URL of the file to be downloaded.

    Returns:
        str: The filename of the downloaded file, or False if there was an error.
    """
    try:
        if url:
            # Extract the filename from the URL
            filename = url.split("/")[-1]

            # Send a GET request to the URL
            response = requests.get(url)

            # Write the response content to a file
            with open(filename, "wb") as file_downloaded:
                file_downloaded.write(response.content)

            return filename
    except ConnectionError as error:
        # Print the error message
        print("Error downloading file:", error)
        return False
