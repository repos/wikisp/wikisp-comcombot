# WikiSP Communications Committee Bot

Telegram bot to send a reminder message to the Wikimedia Small Projects communications committee

[![language: Python](https://img.shields.io/badge/Python%20-v3.10-blue.svg?logo=python)](https://www.python.org) [![License: Apache v2](https://img.shields.io/hexpm/l/apa?logo=apache)](https://www.apache.org/licenses/LICENSE-2.0)

## Installation
* Create a virtual environment for python3:

`$ mkvirtualenv -p python3 wmsp`

* Install requirements:

`$ pip install -r requirements.txt`

## Usage

* Run:

`$ python3 wmsp_bot.py`

## License
[Apache License Version 2.0][def]

[def]: LICENSE
