#!/usr/bin/python3

"""
Telegram bot to send a reminder message to the Wikimedia Small Projects
communications committee

Authors: David Hernández Aponte <david.vzla@gmail.com>
Year: 2023
Version: 2.0
"""
import datetime
import logging

import pytz
from telegram.ext import Application

from modules import birthday, reminder

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)


def main():
    """
    This function sets up and runs a Telegram bot that sends daily reminders and birthday messages.
    """

    # Instantiate the application
    app = (
        Application.builder()
        .token("6039315593:AAFbaFPcoJ5KsIKOQ-nNRpo2-Id60AgOlR8")
        .build()
    )

    # Set up the job queue for reminders
    job_queue_reminder = app.job_queue
    message_time_reminder = datetime.time(
        hour=8, minute=9, tzinfo=pytz.timezone("America/Caracas")
    )

    # Schedule the reminder job
    job_queue_reminder.run_daily(
        callback=reminder.reminder,
        time=message_time_reminder,
        days=(3, 5),
        data=None,
        name=None,
        chat_id=None,
        user_id=None,
        job_kwargs=None,
    )

    # Set up the job queue for birthday messages
    job_queue_birthday = app.job_queue
    message_time_birthday = datetime.time(
        hour=7, minute=41, tzinfo=pytz.timezone("America/Caracas")
    )

    # Schedule the birthday message job
    job_queue_birthday.run_daily(
        callback=birthday.birthday,
        time=message_time_birthday,
        data=None,
        name=None,
        chat_id=None,
        user_id=None,
        job_kwargs=None,
    )

    # Run the application
    app.run_polling()


if __name__ == "__main__":
    main()
